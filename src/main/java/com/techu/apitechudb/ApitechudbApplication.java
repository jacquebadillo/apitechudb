package com.techu.apitechudb;

import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@SpringBootApplication
@ComponentScan({"com.delivery.service","com.delivery.request"})
public class ApitechudbApplication extends SpringBootServletInitializer {

	@Override protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ApitechudbApplication.class);
	}

	public static ArrayList<UserModel> userModel;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.userModel = ApitechudbApplication.getTestData();
	}

	private static ArrayList<UserModel> getTestData() {
		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(new UserModel(
						"1"
						,"Juana"
						,10
				)
		);

		userModels.add(new UserModel(
						"2"
						,"Pedro"
						,8
				)
		);

		userModels.add(new UserModel(
						"3"
						,"Marcela"
						,12
				)
		);

		userModels.add(new UserModel(
						"4"
						,"Leobardo"
						,5
				)
		);

		return userModels;
	}
}
