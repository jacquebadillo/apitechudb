package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll ProductService");

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product) {
        System.out.println("add en ProductService");

        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("En findById de ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel productModel){
        System.out.println("update en productService");

        return this.productRepository.save(productModel);

    }
    public boolean delete(String id) {
        System.out.println("Delete en ProductService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Producto encontrado borrado");
            this.productRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}