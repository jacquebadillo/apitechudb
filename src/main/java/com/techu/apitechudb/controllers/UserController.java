package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    //    static final String APIBaseUrl = "/apitechu/v2";
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(name = "$orderby", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderby es: " + orderBy);

        return new ResponseEntity<>(
                this.userService.getUsers(orderBy),
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById ");
        System.out.println("La id a consultar es: " + id);

//        UserModel result = new UserModel();
//        for (UserModel user : ApitechudbApplication.userModel) {
//            if (user.getId().equals(id)) {
//                result = user;
        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "No se encuentra usuario",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> createUser(@RequestBody UserModel newUser) {
        System.out.println("createUser ");
        System.out.println("La id del nuevo usuario es: " + newUser.getId());
        System.out.println("El nombre de nuevo usuario es: " + newUser.getName());
        System.out.println("La edad del nuevo usuario es: " + newUser.getAge());

        ApitechudbApplication.userModel.add(newUser);

        return new ResponseEntity<>(
                this.userService.createUser(newUser),
//                newUser,
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser ");
        System.out.println("La id del usuario a actualizar en URL es: " + id);
        System.out.println("La id del usuario a actualizar es: " + user.getId());
        System.out.println("El nombre de usuario a actualizar es: " + user.getName());
        System.out.println("La edad del usuario a actualizar es: " + user.getAge());

//        for (UserModel userInList : ApitechudbApplication.userModel) {
//            if (userInList.getId().equals(id)){
//                userInList.setId(user.getId());
//                userInList.setName(user.getName());
//                userInList.setAge(user.getAge());
//            }
        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("Actualizando usuario");
            this.userService.update(user);
        }
        return new ResponseEntity<>(
                user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
//        return user;

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteProduct ");
        System.out.println("La id del producto a borrar es: " + id);

//        UserModel result = new UserModel();
//        boolean foundUser = false;
        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<> (
                deleteUser ? "Usuarios borrado" : "Usuario no encontrado, no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

//        for (UserModel userInList : ApitechudbApplication.userModel)
//            if (userInList.getId().equals(id)) {
//                System.out.println("Usario encontrado: " + id);
//                foundUser = true;
//                result = userInList;
//            }
//        if (foundUser == true) {
//           System.out.println("Borrando usuario: " + id);
//            ApitechudbApplication.userModel.remove(result);
//        }
//        return result;
//      }

    @PatchMapping("/users/{id}")
    public UserModel updateparModel (@RequestBody UserModel userData, @PathVariable String id) {
        System.out.println("updateparModel ");
        System.out.println("La id en URL del usuario a actualizar es: " + id);
        System.out.println("El nombre del usuario a actualizar es: " + userData.getName());
        System.out.println("La edad del usuario a actualizar es: " + userData.getAge());

        UserModel result = new UserModel();

        for(UserModel userInList : ApitechudbApplication.userModel) {
            if(userInList.getId().equals(id)) {
                result = userInList;
                if(userData.getName() != null ) {
                    System.out.println("Actualizando el nombre" + userData.getName());
                    userInList.setName(userData.getName());
                }
                if (userData.getAge() > 0) {
                    System.out.println("Actualizando la edad " +userData.getAge());
                    userInList.setAge(userData.getAge());
                }
            }
        }
        return result;
    }
}
